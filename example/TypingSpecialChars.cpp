#include "Arduino.h"
#include "KeyboardLayout_DE.h"
#include "USBHIDKeyboardMod.h"
#include "USB.h"


KeyboardLayout *layout = new KeyboardLayoutDE();
USBHIDKeyboardMod keyboard;

void setup()
{
    delay(1000);
    bool ready = true;
    ready &= USB.begin();
    keyboard.begin(layout);
    delay(1000);
    keyboard.write(u"This is a string with special chars like this ä,Ö,ü and this one ß.");
    // keyboard.write(u"This is a string with special chars like this ä,Ö,ü and this one ß.");
    // Not working since not all special chars are implemented so far
    //keyboard.write(u"°!\"§$%&/()=?`QWERTZUIOPÜ*'asdfghjklöäYXCVBNM;:_YXCVBNM;:_ >");
    //keyboard.write(u"′¹²³¼½¬{[]}\\¸@ſ€¶ŧ←↓→øþ¨~’æſðđŋħ ̣ĸł˝^»«¢„“”µ·…–»«¢„“”µ·…– |");
}

void loop()
{

}